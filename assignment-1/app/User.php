<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Article;
use App\Models\Comment;
use App\User;

class User extends Authenticatable
{
    use Notifiable;

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
    * The attributes that should be hidden for arrays.
    *
    * @var array
    */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function likes()
    {
        return $this->belongsToMany(Tweet::class);
    }

    public function article()
    {
        return $this->hasMany(Article::class);
    }

    public function comment()
    {
        return $this->hasMany(Comment::class);
    }
}
