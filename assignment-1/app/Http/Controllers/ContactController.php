<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contact;

class ContactController extends Controller
{
    public function create() {
        return view('contact');
    }

    public function store() {

        $request = request();

        $result = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'message' => 'required'
        ], [
            'name.required' => 'Please Enter a Name.',
            'email.required' => 'Please Enter an Email.',
            'email.email' => 'Please Enter a Valid Email.',
            'message.required' => 'Please Enter a Message.'
        ]);

        $data = request()->all();

        $contact = new Contact();
        $contact->name = $data['name'];
        $contact->email = $data['email'];
        $contact->message = $data['message'];
        $contact->save();

        return redirect('/contact')->with('message', 'You should recieve an email from us shortly!');
    }
}
