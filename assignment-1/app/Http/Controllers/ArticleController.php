<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Article;
use App\User;

class ArticleController extends Controller
{
    public function create() {

        if (!\Auth::check()) {
            return redirect('/login');
        }

        return view('article');
    }

    public function store() {

        $request = request();
        $loggedInUser = $request->user();
        $data = request()->all();

        $result = $request->validate([
            'title' => 'required|max:255',
            'punchline' => 'required|max:255',
            'category' => 'required|max:255',
            'text' => 'required',
            // 'image' => 'image|nullable|max:1999'
        ], [
            'title.required' => 'Please enter a title.',
            'punchline.required' => 'Please enter a punchline.',
            'category.required' => 'Please enter a category.',
            'text.required' => 'Please enter some content.'
        ]);

        //  // File Upload
        // if($request->hasFile('image')){
        //     $filenameWithExt = $request->file('image')->getClientOriginalName();
        //     $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        //     $extension = $request->file('image')->getClientOriginalExtension();
        //     $fileNameToStore= $filename.'_'.time().'.'.$extension;
        //     $path = $request->file('image')->storeAs('public/articleImages', $fileNameToStore);
        // }

        // else {
        //     $fileNameToStore = 'noimage.jpg';
        // }

        $article = new Article();
        $article->user_id = $loggedInUser->id;

        // $article->image = $data['image'];
        $article->punchline = $data['punchline'];
        $article->title = $data['title'];
        $article->category = $data['category'];
        $article->text = $data['text'];
        $article->save();

        return redirect('/')->with('message', 'Your Article was successfully posted!');
    }

    public function userArticles($userId) {

        $user = User::where('name', $userId)
        ->orWhere('id', $userId)
        ->first();

        if (!$user) {
            abort(404);
        }

        $articles = $user->article;

        return view('userArticles', [
        'user' => $user,
        'articles' => $articles
        ]);
    }

    public function toggleLike($articleId) {

        $user = request()->user();
        $article = Article::find($articleId);

        if ($article->isLikedByCurrentUser()) {
            $article->likes()->detach($user);
        } else {
            $article->likes()->attach($user);
        }

        return back()->with('message', 'You liked / unliked an article!');
    }
}
