<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@create');

/*--------------------------------------------------------------------------
'Hero' Article
--------------------------------------------------------------------------*/

Route::get('/article', 'ArticleController@create');
Route::post('/article', 'ArticleController@store');
Route::get('/article/{id}', 'ArticleController@userArticles');

/*--------------------------------------------------------------------------
Sub Article
--------------------------------------------------------------------------*/

//--------------------------------------------------------------------------
// Likes
//--------------------------------------------------------------------------

Route::get('/articles/{id}/like/toggle', 'ArticleController@toggleLike');

/*--------------------------------------------------------------------------
Comment
--------------------------------------------------------------------------*/

Route::get('/comment', 'CommentController@create');
Route::post('/comment', 'CommentController@store');

//--------------------------------------------------------------------------
// Contact
//--------------------------------------------------------------------------

Route::get('/contact', 'ContactController@create');
Route::post('/contact', 'ContactController@store');

// class subArticle {}


// $faker = Factory::create();

/*--------------------------------------------------------------------------
Sub Article's
--------------------------------------------------------------------------*/

// $subarticle = new subArticle();
// $subarticle->articleImage = $faker->imageUrl();
// $subarticle->articlePunchline = $faker->sentence($nbWords = 2, $variableNbWords = false);
// $subarticle->articleTitle = $faker->Text($maxNbChars = 50, $indexSize = 2);
// $subarticle->diggs = $faker->randomNumber($nbDigits = 2, $strict = false);
// $subarticle->articleCategory = $faker->sentence($nbWords = 1, $variableNbWords = false);
// $subarticle->articleText = $faker->realText($maxNbChars = 220, $indexSize = 2);
//
// $subarticle2 = new subArticle();
// $subarticle2->articleImage = $faker->imageUrl();
// $subarticle2->articlePunchline = $faker->sentence($nbWords = 2, $variableNbWords = false);
// $subarticle2->articleTitle = $faker->Text($maxNbChars = 50, $indexSize = 2);
// $subarticle2->diggs = $faker->randomNumber($nbDigits = 2, $strict = false);
// $subarticle2->articleCategory = $faker->sentence($nbWords = 1, $variableNbWords = false);
// $subarticle2->articleText = $faker->realText($maxNbChars = 220, $indexSize = 2);
//
// $subarticle3 = new subArticle();
// $subarticle3->articleImage = $faker->imageUrl();
// $subarticle3->articlePunchline = $faker->sentence($nbWords = 2, $variableNbWords = false);
// $subarticle3->articleTitle = $faker->Text($maxNbChars = 50, $indexSize = 2);
// $subarticle3->diggs = $faker->randomNumber($nbDigits = 2, $strict = false);
// $subarticle3->articleCategory = $faker->sentence($nbWords = 1, $variableNbWords = false);
// $subarticle3->articleText = $faker->realText($maxNbChars = 220, $indexSize = 2);

/*------------------------------------------------------------------------*/

// $subarticles = [$subarticle, $subarticle2, $subarticle3,];
//
// $data = [
//   'subarticles' => [$subarticle, $subarticle2, $subarticle3,]
// ];

/*--------------------------------------------------------------------------
Auth
--------------------------------------------------------------------------*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
