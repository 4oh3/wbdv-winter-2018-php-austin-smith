<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/css/app.css">
    <title>Digg - Austin Smith</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,800|Titillium+Web:300,400" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>

    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    NavBar
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <section class="navBar">
        <ul>
            <li><a href="/"><svg class="home" viewBox="0 0 48 48"<g fill="none" fill-rule="evenodd"><path fill="#000" d="M6.03 23.958H0L24 0l24 23.958h-6.03V46H6.03V23.958zM20 46h8V30h-8v16z"></path></g></svg><span class="iconPad">Home</span></a></li>

            <li><a href="#"><svg class="video" viewBox="0 0 48 48"><g fill="none" fill-rule="evenodd"><path fill="#000" d="M8 2v44l37-22z"></path></g></svg><span class="iconPad">Video</span></a></li>

            <li><a href="#"><svg class="editions" viewBox="0 0 12 12"><path d="M2 1.047L11 1v9.43c0 .717-.2.97-1 .97H2c-.8 0-1-.249-1-.97V2.981h8v6.56c0 .229.166.52.507.52.34 0 .493-.292.493-.52V2.044H2v-.997z" fill-rule="evenodd"></path></svg><span class="iconPad">Editions</span></a></li>

            <li><a href="#"><svg class="store" viewBox="0 0 48 48"><g fill="none" fill-rule="evenodd"><path fill="#000" d="M36 12h8v36H4V12h8c0-6.627 5.373-12 12-12 6.626 0 12 5.373 12 12zm-20 0h16c0-4.417-3.583-8-8-8-4.418 0-8 3.583-8 8z"></path></g></svg><span class="iconPad">Store</span></a></li>

            <li><a href="/contact"><i class="fa fa-envelope" aria-hidden="true"></i>Contact</a></li>

            <li class="logo"><a href="/"><svg viewBox="0 0 47 27"><path d="M5.025 10.025H7V17H5.025v-6.975zM0 21h12V0H7v6H0v15zM14 6h5v15h-5zm0-6h5v4h-5zm12 10h2v7h-2v-7zm-5 11h7v2h-7v4h12V6H21v15zm19-11h2v7h-2v-7zm-5 11h7v2h-7v4h12V6H35v15z" fill="#1B1A19" fill-rule="evenodd"></path></svg></a></li>


            <li><a href="#"><svg class="search" viewBox="0 0 48 48"><g fill="none" fill-rule="evenodd"><path fill="#000" d="M21.038 1.4c8.284 0 15 6.717 15 15 0 8.286-6.716 15-15 15-8.284 0-15-6.714-15-15 0-8.283 6.716-15 15-15m5.153 32.146L34.555 46.7l6.523-4.147-8.132-12.796c-1.925 1.718-4.222 3.026-6.753 3.79z"></path></g></svg><span class="iconPad">Search</span></a>
            </li>

            <li>
                @if (Auth::check())
                    <a href="/article"><li><i class="fa fa-plus" aria-hidden="true"></i>Article</li></a>
                @endif
            </li>

            <li>
                @if (Auth::check())
                    <a href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();"><li><button>Logout</button>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                @else
                    <a href="/login"><li><button>Sign In / Sign Up</button></li></a>
                @endif
            </li>

            <li>
                @if (Auth::check())
                    Hello, {{ Auth::user()->name }}
                @endif
            </li>
        </ul>
    </section>

    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    End NavBar
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <?php if($message = session('message')): ?>
        <div class="alert alert-success">
            <?php echo $message ?>
        </div>
    <?php endif; ?>

    <?php if($errors->any()): ?>
        <?php foreach($errors->all() as $error): ?>
            <div class="alert alert-danger"><?php echo $error ?></div>
        <?php endforeach; ?>
    <?php endif; ?>

    @yield('content')

</body>
</html>
