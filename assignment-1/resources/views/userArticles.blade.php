@extends('layout')

@section('content')

    <h1 class="userArticles">{{ $user->name }}'s Articles</h1>

    <ul>
        @foreach ($articles as $article)
            <div class="mainArticle">

                <!-- <img class="mainArticleImage" src="<?php echo $article->image ?>"> -->

                <div class="mainAritcleContent">

                    <div class="articlePunchline">
                        <p><?php echo $article->punchline ?></p>
                    </div>

                    <div class="articleTitle">
                        <h1><?php echo $article->title ?></h1>
                    </div>

                    <div class="articleSnippets">
                        <p><span><?php echo count($article->likes) ?></span> diggs</p>
                        <p><a href="#"><?php echo $article->category ?></a></p>
                        <p><a href="/article/<?php echo $article->user->name ?>"><?php echo $article->user->name ?></a></p>
                    </div>

                    <div class="articleText">
                        <p><?php echo $article->text ?></p>
                    </div>

                    <div class="articleIcons">
                        <span class="numberLikes"></span>
                        <?php echo count($article->likes) ?>
                        @if ($article->isLikedByCurrentUser())
                            <a href="/articles/{{ $article->id }}/like/toggle"><i class="fa fa-heart-o"></i></a>
                        @else
                            <a href="/articles/{{ $article->id }}/like/toggle"><i class="fa fa-heart-o"></i></a>
                        @endif
                        <a  href="#"><i class="fa fa-bookmark-o" aria-hidden="true"></i></a>
                        <a  href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a  href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                    </div>
                    <br>

                    <?php foreach ($article->comment as $comment): ?>
                        <span class="name">Comments:</span> <?php echo $comment->content ?><br>
                        <span class="name">User:</span> <?php echo $comment->user->name ?> <br>
                        <span class="name">Date:</span> <?php echo $comment->updated_at->diffForHumans() ?>
                    <?php endforeach; ?>

                    <form method="post" action="/comment">
                        <?php echo csrf_field() ?>
                        <input type="text" name="comment" placeholder="Comment">
                        <input type="hidden" name="articleId" value="{{ $article->id }}">
                        <br>
                        <br>
                        <input type="submit" name="" value="Post Comment" class="btn btn-primary">
                    </form>

                </div>

            </div>
        @endforeach
    </ul>
@endsection
