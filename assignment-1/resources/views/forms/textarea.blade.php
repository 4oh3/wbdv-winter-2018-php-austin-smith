<div class="form-group">

    <textarea
    name={{ $name }}
    rows="4"
    cols="50"
    placeholder={{ $label }}
    class="form-control col-md-4 .invalid-feedback {{ $errors->has('message') ? 'is-invalid' : '' }}">{{ old($name) }}</textarea>

    @if($errors->has($name))
        <span class="invalid-feedback">{{ $errors->first($name) }}</span>
    @endif

</div>
