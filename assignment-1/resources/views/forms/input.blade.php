<div class="form-group">

    <input
    type="text"
    name="{{ $name }}"
    placeholder="{{ $label }}"
    value="{{ old($name) }}"
    class="form-control col-md-4 {{ $errors->has($name) ? 'is-invalid' : '' }}">
    
    @if($errors->has($name))
        <span class="invalid-feedback">{{ $errors->first($name) }}</span>
    @endif

</div>
