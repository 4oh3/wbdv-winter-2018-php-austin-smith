@extends('layout')

@section('content')

    <h3 class="articleTitle">Create Article</h3>

    <div class="form-group">

        <form method="post">
            {{ csrf_field() }}

            <label for="title">Title:</label>
            <input type="text" name="title" value="{{ old('title') }}" class="form-control col-md-4 {{ $errors->has('title') ? 'is-invalid' : '' }}">
            @if($errors->has('title'))
                <span class="invalid-feedback">{{ $errors->first('title') }}</span>
            @else
                <small class="form-text text-muted">Example - "How to Escape North Korea"</small>
            @endif

            <label for="type">Punchline:</label>
            <input type="text" name="punchline" value="{{ old('punchline') }}" class="form-control col-md-4">
            @if($errors->has('punchline'))
                <span class="invalid-feedback">{{ $errors->first('punchline') }}</span>
            @else
                <small class="form-text text-muted">Example - "No.1 Don't go to Russia"</small>
            @endif

            <label for="category">Category:</label>
            <input type="text" name="category" value="{{ old('category') }}"="" class="form-control col-md-4">
            @if($errors->has('category'))
                <span class="invalid-feedback">{{ $errors->first('category') }}</span>
            @else
                <small class="form-text text-muted">Example - North Korea</small>
            @endif

            {{-- <label for="image">Upload an Image:</label>
            <input type="file" name="image" value="" accept="image/*" class="form-control col-md-4"> --}}

            <label for="text">Content:</label>
            <textarea name="text" rows="5" cols="80" value="{{ old('text') }}" class="form-control col-md-4"></textarea>
            @if($errors->has('text'))
                <span class="invalid-feedback">{{ $errors->first('text') }}</span>
            @else
                <small class="form-text text-muted">Write your heart out.</small>
            @endif

            <input type="submit" name="" value="Post Article" class="btn btn-primary">
        </form>

        {{-- @if($errors->has($name))
        <span class="invalid-feedback">{{ $errors->first($name) }}</span>
    @endif --}}

</div>

@endsection
