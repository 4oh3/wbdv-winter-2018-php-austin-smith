@extends('layout')

@section('content')

<?php if($message = session('message')): ?>
    <div class="alert alert-success">
        <?php echo $message ?>
    </div>
<?php endif; ?>

<!-- <?php if($errors->any()): ?>
<div class="alert alert-danger">
<ul>
<?php foreach($errors->all() as $error): ?>
<li><?php echo $error ?></li>

<?php endforeach; ?>
</ul>
</div>
<?php endif; ?> -->

<h1>Contact Form</h1><br>

<form method="post">
    <?php echo csrf_field() ?>

    <div class="contactForm">

        <h2>Let's Get in Touch!</h2>

        @include ('forms.input', [
            'label' => 'Name',
            'name' => 'name'
        ])

        @include ('forms.input', [
            'label' => 'Email',
            'name' => 'email'
        ])

        @include ('forms.textarea', [
            'label' => 'Message',
            'name' => 'message'
        ])

        <input type="submit" name="" value="Submit" class="btn btn-primary <?php echo $errors->all() ? 'btn-danger' : '' ?>">

    </div>

</form>

@endsection
