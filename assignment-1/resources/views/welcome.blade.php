@extends('layout')

@section('content')
    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Sub NavBar
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <section class="subNav">
        <a href="#">Technology</a>
        <a href="#">Long Reads</a>
        <a href="#">Science</a>
        <a href="#">Donald Trump</a>
        <a href="#">News</a>
        <a href="#">Entertainment</a>
        <a href="#">Digg Features</a>
        <a href="#">Originals</a>
    </section>

    <!-- Wrapper for main content -->

    <section class="mainContent">

        <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        'Hero' Article
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

        <?php foreach ($articles as $article): ?>
            <div class="mainArticle">

                <!-- <img class="mainArticleImage" src="/storage/articleImages/<?php echo $article->image ?>"> -->

                <div class="mainAritcleContent">

                    <div class="articlePunchline">
                        <p><?php echo $article->punchline ?></p>
                    </div>

                    <div class="articleTitle">
                        <h1><?php echo $article->title ?></h1>
                    </div>

                    <div class="articleSnippets">
                        <p><span><?php echo count($article->likes) ?></span> diggs</p>
                        <p><a href="#"><?php echo $article->category ?></a></p>
                        <p><a href="/article/<?php echo $article->user->name ?>"><?php echo $article->user->name ?></a></p>
                    </div>

                    <div class="articleText">
                        <p><?php echo $article->text ?></p>
                    </div>

                    <div class="articleIcons">
                        <span class="numberLikes"></span>
                        <?php echo count($article->likes) ?>
                        @if ($article->isLikedByCurrentUser())
                            <a href="/articles/{{ $article->id }}/like/toggle"><i class="fa fa-heart-o"></i></a>
                        @else
                            <a href="/articles/{{ $article->id }}/like/toggle"><i class="fa fa-heart-o"></i></a>
                        @endif
                        <a  href="#"><i class="fa fa-bookmark-o" aria-hidden="true"></i></a>
                        <a  href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a  href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                    </div>
                    <br>

                    <?php foreach ($article->comment as $comment): ?>
                        <hr>
                        <span class="name">Comment:</span> <?php echo $comment->content ?><br>
                        <span class="name">User:</span> <?php echo $comment->user->name ?> <br>
                        <span class="name">Date:</span> <?php echo $comment->updated_at->diffForHumans() ?>
                        <hr>
                    <?php endforeach; ?>

                    <form method="post" action="/comment">
                        <?php echo csrf_field() ?>
                        <input type="text" name="comment" placeholder="Comment" class="form-control">
                        <input type="hidden" name="articleId" value="{{ $article->id }}">
                        <br>
                        <input type="submit" name="" value="Post Comment" class="btn btn-primary">
                    </form>

                </div>

            </div>
        <?php endforeach; ?>

        <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Sub Articles
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->



        <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        End Sub Articles
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

        <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Footer
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

        <section class="footer">
            <a href="#">About</a>
            <a href="#">FAQ</a>
            <a href="#">Blog</a>
            <a href="#">Jobs</a>
            <a href="#">Advertise</a>
            <a href="#">Submit a Link</a>
            <a href="#">Community Guidelines</a>
            <a href="#">Terms</a>
            <a href="#">Privacy</a>
            <a>© 2018 Digg Inc.</a>
        </section>

        <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        End Footer
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

        <!-- End Wrapper -->

    </section>

@endsection
